module.exports = {
  purge: ['./src/**/*.{ts,tsx}'],
  darkMode: 'media',
  theme: {
    fontFamily: {
      serif:
        "'Source Serif Pro', -apple-system, BlinkMacSystemFont, Segoe UI, 'Helvetica Neue', sans-serif",
      sans: "Inter, -apple-system, BlinkMacSystemFont, Segoe UI, 'Helvetica Neue', sans-serif",
      mono: "monospace",
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}

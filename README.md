# Remote Concerns

The faint echoes of a scream fade away, leaving you utterly alone in the desert.

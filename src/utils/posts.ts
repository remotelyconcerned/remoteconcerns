import { promises as fs } from 'fs'

const pagesDirectory = './src/pages'

export type PostMetadata = {
  title: string
  date: string
  slug: string
}

export async function getPosts() {
  const pages = await fs.readdir(pagesDirectory)
  const files = pages.filter(filename => filename.match(/.*\.mdx$/gi))
  const posts: PostMetadata[] = await Promise.all(
    files.map(async filename => {
      const post = await import(`../pages/${filename}`)
      return {
        ...post.meta,
        slug: filename.replace('.mdx', ''),
      }
    })
  )
  posts.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime())

  return posts
}

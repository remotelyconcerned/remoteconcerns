import Head from 'next/head'

const description = 'Remote concerns of a recursing botanist.'

type MetaProps = {
  title: string
}

const Meta: React.FC<MetaProps> = ({ title }) => (
  <Head>
    <title>{title} &middot; remote concerns</title>

    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="anonymous" />
    <link
      href="https://fonts.googleapis.com/css2?family=Inter:wght@400;700&family=Source+Serif+Pro:ital@0;1&display=swap"
      rel="stylesheet"
    />

    <meta name="description" content={description} />
    <meta name="viewport" content="width=device-width" />

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
    <link rel="manifest" href="/site.webmanifest" />
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
    <meta name="msapplication-TileColor" content="#da532c" />
    <meta name="theme-color" content="#ffffff" />

    <meta property="og:title" content="Remote Concerns" />
    {process.env.NEXT_PUBLIC_BASE_URL && (
      <meta property="og:url" content={process.env.NEXT_PUBLIC_BASE_URL} />
    )}
    <meta property="og:type" content="article" />
    <meta property="og:description" content={description} />
    {process.env.NEXT_PUBLIC_BASE_URL && (
      <meta
        property="og:image"
        content={`${process.env.NEXT_PUBLIC_BASE_URL}/android-chrome-192x192.png`}
      />
    )}
  </Head>
)

export default Meta

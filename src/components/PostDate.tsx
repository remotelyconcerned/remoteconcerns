type PostDateProps = {
  date: string
}

const PostDate: React.FC<PostDateProps> = ({ date }) => (
  <p className="font-sans text-gray-500 text-xs mb-8">
    {new Date(date).toLocaleDateString()}
  </p>
)

export default PostDate

import Link from 'next/link'

const Header: React.FC = () => (
  <header className="font-sans text-gray-400 mb-12">
    <nav className="flex flex-wrap">
      <Link href="/">
        <a>remote&nbsp;concerns</a>
      </Link>
    </nav>
  </header>
)

export default Header

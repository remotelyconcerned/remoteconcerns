import Link from 'next/link'
import { PostMetadata } from '../utils/posts'

type PostListProps = {
  posts: PostMetadata[]
}

const PostList: React.FC<PostListProps> = ({ posts }) => (
  <ul className="mb-16">
    {posts.map(post => (
      <li key={post.slug} className="mb-2">
        <Link href={`/${post.slug}`}>
          <a className="no-underline">
            <span className="font-sans text-sm text-gray-800 mr-4">
              {new Date(post.date).toLocaleDateString()}
            </span>
            <span className="underline">{post.title}</span>
          </a>
        </Link>
      </li>
    ))}
  </ul>
)

export default PostList

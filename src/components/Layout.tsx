import Footer from './Footer'
import Header from './Header'
import Meta from './Meta'

type PageMetadata = {
  title: string
}

type LayoutProps = {
  meta: PageMetadata
}

const Layout: React.FC<LayoutProps> = ({ meta, children }) => (
  <div className="m-4 sm:m-8 xl:ml-16">
    <Meta title={meta.title} />
    <Header />
    <main className="container max-w-xl">{children}</main>
    <Footer />
  </div>
)

export default Layout

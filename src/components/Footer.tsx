import Link from 'next/link'

const Footer: React.FC = () => (
  <footer className="font-sans text-gray-400 mt-8">
    <div>
      <Link href="/">
        <a>remote concerns</a>
      </Link>{' '}
      of a recursing botanist
    </div>
  </footer>
)

export default Footer

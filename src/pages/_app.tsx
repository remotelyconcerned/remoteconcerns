import { AppProps } from 'next/app'
import '../styles/globals.css'

function RemoteConcernsApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

export default RemoteConcernsApp

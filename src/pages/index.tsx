import { GetStaticProps } from 'next'
import Layout from '../components/Layout'
import PostList from '../components/PostList'
import { getPosts, PostMetadata } from '../utils/posts'

type LandingPageProps = {
  posts: PostMetadata[]
}

const LandingPage: React.FC<LandingPageProps> = ({ posts }) => (
  <Layout meta={{ title: 'index' }}>
    <h1 className="mb-8">Remote Concerns</h1>
    <p>Imagine a place where thoughts go to get out of your head.</p>
    <p>
      A place hidden away, hidden from everyone in plain sight. A place only you know exists. An
      extension of the mind. A vacuum where thoughts can be suspended and ripen unhurried, unjudged.
    </p>
    <p className="mb-16">This is my such place.</p>
    <PostList posts={posts} />
  </Layout>
)

export default LandingPage

export const getStaticProps: GetStaticProps<LandingPageProps> = async ctx => ({
  props: {
    posts: await getPosts(),
  },
})
